var STORAGENAME = "bernhard-maier.net_UID";
var socket;
var activeDIV = "";
var previousDIV = "";

$(document).ready(function(){
	
	socket = io.connect();
	
	socket.on("connect", function(){
		socket.emit("connectSite", localStorage.getItem(STORAGENAME));
	});
	
	socket.on("connected", function(uid){
		localStorage.setItem(STORAGENAME, uid);
	});
		
	initialize();
	loadContent();
	fitToScreen();
	
	activeDIV = "StartDIV";
	previousDIV = "NONE";
	showDIV(activeDIV);
	highlightActiveNavButton(activeDIV, previousDIV);	
	
	$(this).tooltip({
		//show: { effect: "fadeIn", duration: 250 },
		//hide: { effect: "fadeOut", duration: 250 },
		show: { effect: "scale", duration: 100, origin: ["top", "left"], direction: "both", scale: "box", percent: 100 },
		hide: { effect: "scale", duration: 100, origin: ["top", "left"], direction: "both", scale: "box", percent: 0 },
		position: { my: "left-5 top-1", at: "left bottom" },
		//track: true,
		content: function(){
			return $(this).attr('title');
		}
	});
	
	
	// #############################
	// ### local eventlisteners ###
	// #############################
	
	$(window).resize(function(){
		fitToScreen();
	});

    $(".navButton").click(function() {
		previousDIV = activeDIV;
		activeDIV = $(this).text()+"DIV";
		showDIV(activeDIV);
		highlightActiveNavButton(activeDIV, previousDIV);
    });
	
    $(".backToTop").click(function() {
		alert("clicked");
		$('html, body').animate({scrollTop: 0}, '500', 'swing', function(){
			alert("on top");
		});
    });
	
	$("p.navButton").mouseover(function(){
		var buttonID = $(this).text()+"DIV";
		if(buttonID != activeDIV){
			highlightNavButtonSoft($(this));
		}
	});
	
	$("p.navButton").mouseout(function(){
		var buttonID = $(this).text()+"DIV";
		if(buttonID != activeDIV){
			unhighlightNavButtonSoft($(this));
		}
	});
	
	$("div.navButton").mouseover(function(){
		$(this).css("border-color", "#FF0000");
	});
	
	$("div.navButton").mouseout(function(){
		$(this).css("border-color", "#FFFFFF");
	});

	
	// #############################
	// ### remote eventlisteners ###
	// #############################
	
	socket.on("connect", function(){
		socket.emit("connected");
	});

	socket.on("msg", function(msg){
		alert(msg);
	});

});


// #################
// ### functions ###
// #################

function initialize(){
	$('#info').css("display", "none");
	$('#canvas').css("display", "block");
}

function loadContent(){
	$("#StartDIV").load("content/start.html");
	$("#AboutDIV").load("content/about.html");
	$("#PortfolioDIV").load("content/portfolio.html");
	$("#ContactDIV").load("content/contact.html");
	$("#ImpressumDIV").load("content/impressum.html");
	$("#DatenschutzDIV").load("content/datenschutz.html");
	$("#DownloadsDIV").load("content/downloads.html");	
}

function fitToScreen(){
	var mainDIV = document.getElementById("mainDIV");
	var navDIV = document.getElementById("navDIV");
	
	var mainDIVborderWidth = 5;
	var newInnerWidth = window.innerWidth*0.85;
	var newInnerHeight = window.innerHeight*0.85;
	var navDIVwidth = 135;
	
	mainDIV.style.borderWidth = mainDIVborderWidth + "px";
	mainDIV.style.left = (window.innerWidth-newInnerWidth)/2 - mainDIVborderWidth + "px";
	mainDIV.style.top = (window.innerHeight-newInnerHeight)/2 - mainDIVborderWidth + "px";
	mainDIV.style.width = newInnerWidth + "px";
	mainDIV.style.height = newInnerHeight + "px";
	
	navDIV.style.left = 0 + "px";
	navDIV.style.top = 0 + "px";
	//navDIV.style.top = (newInnerHeight-navDIV.offsetHeight)/2 + "px";
	navDIV.style.width = navDIVwidth + "px";
	//navDIV.style.height = newInnerHeight + "px";
	
	var left = 0 + navDIVwidth + "px";
	var top = 0 + "px";
	var width = newInnerWidth - navDIVwidth + "px";
	var height = newInnerHeight + "px";
	
	$(".contentCanvas").each(function(){
		this.style.top = top;
		this.style.left = left;
		this.style.width = width;
		this.style.height = height;
	});
}

function checkForScrolling(){
	$('div.contentCanvas').each(function(){
		if(this.offsetHeight < this.scrollHeight){
			this.css("overflow-y","scroll"); //not working properly: checking is fine, setting propery not
		}
	});
}

function highlightNavButton(elem){
	elem.width(110);
	elem.css("border-right-color", "#FF0000");
}

function unhighlightNavButton(elem){
	elem.width(60);
	elem.css("border-right-color", "#000000");
}

function highlightNavButtonSoft(elem){
	elem.animate(
	{width: 110},
	{duration: 400, easing: 'easeOutBack', complete: function(){
		elem.css("border-right-color", "#FF0000");
	}});
}

function unhighlightNavButtonSoft(elem){
	elem.animate(
	{width: 60},
	{duration: 400, easing: 'easeOutBack', start: function(){
		elem.css("border-right-color", "#000000");
	}});
}

function highlightActiveNavButton(activeDIV, previousDIV){
	$("p.navButton").each(function(){
		var buttonID = $(this).text()+"DIV";
		if(buttonID == activeDIV){
			highlightNavButton($(this));
		} else if(buttonID == previousDIV) {
			unhighlightNavButtonSoft($(this));
		} else {
			unhighlightNavButton($(this));
		}
	});
}

function showDIV(DIV){
	pauseAllVideos();
	$(".contentCanvas").each(function(){
		$(this).fadeOut(150, function(){
			this.style.display = "none";
		});
	});
	$("#"+DIV).fadeIn(250, function(){
		this.style.display = "block";
	});
}

function pauseAllVideos(){
	$("video").each(function(){
		this.pause();
	});
}