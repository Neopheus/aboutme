var express = require("express");
var app = express();
var server = require("http").createServer(app);
var io = require("socket.io").listen(server);
var config = require("./config.json");
var fs = require("fs");

server.listen(config.port);
app.use(express.static(__dirname + "/public"));
app.get("/", function(req, res){
	res.sendFile(__dirname + "/public/index.html");
});

var DEBUG = false;
var USER = {};

console.log(getDate(new Date()));

io.sockets.on("connection", function(socket){
	
	var user = {};
	
	socket.on("disconnect", function(){
		console.log("disconnect...");
		user.disconnected = true;
		setTimeout(function(){
			if(user.disconnected){
				delete USER[user.id];
				console.log("...disconnected!");
			} else {
				console.log("...reconnected!");
			}
		}, config.TIMEOUTSECS*1000);
	});
	
	socket.on("connectSite", function(uid){
		if(uid!=null && USER.hasOwnProperty(uid)){
			user = USER[uid];
			user.disconnected = false;
		} else {
			user.id = newId();
			USER[user.id] = user;
			console.log("New User on Site.");
			log("visitors", socket.request.connection.remoteAddress+" ("+user.id+")");
			//log("visitors", socket.handshake.address+" ("+user.id+")");
			socket.emit("connected", user.id);
		}
	});
	
	function message(msg){
		console.log(msg);
		socket.emit("msg", msg);
	}

});

function newId(){
	var randomgeneratedUID = Math.random().toString(36).substring(3,16);
	return randomgeneratedUID;
}

function log(file, msg){
	var entry = "-> "+getDate(new Date())+"   :   "+msg+"\r\n";
	fs.appendFileSync("./logs/"+file+".log", entry, "utf8");
}

function getDate(date){
	var Y = date.getFullYear();
	var M = date.getMonth()+1;
	while(M.length<2) "0"+M;
	var D = date.getDate();
	while(D.length<2) "0"+D;
	var h = date.getHours();
	while(h.length<2) "0"+h;
	var m = date.getMinutes();
	while(m.length<2) "0"+m;
	var s = date.getSeconds();
	while(s.length<3) "0"+s;
	var ms = date.getMilliseconds();
	while(ms.length<3) "0"+ms;
	return Y+"/"+M+"/"+D+"-"+h+":"+m+":"+s+":"+ms;
}


//###################
//### END OF CODE ###
//###################

console.log("###################################################");
console.log("### Server is running on http://127.0.0.1:" + config.port + "/ ###");
console.log("###################################################");