function createInfoButton(buttonChar, buttontitle, popuptext){
	var infoButton = $("<div id='infoButton' title='"+buttontitle+"'>"+buttonChar[0]+"</div>");
	infoButton.css({
		"position": "fixed",
	  "bottom": "15px",
	  "right": "15px",
	  "width": "30px",
	  "height": "30px",
	  "background-color": "black",
	  "color": "white",
		"font-family": "Arial, Helvetica, sans-serif",
	  "font-size": "25px",
	  "font-weight": "bold",
    "text-shadow": "none",
	  "text-align": "center",
	  "border": "3px solid white",
	  "border-radius": "20px"
	});
	infoButton.click(function(){
		alert(popuptext);
	});
	$("body").prepend(infoButton);
}
