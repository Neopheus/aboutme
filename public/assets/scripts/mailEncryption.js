function encryptMail(mail){
	var encrypted = "";
	for(var i=0; i<mail.length; i++){
		var c = mail.charCodeAt(i);
		c = (c>=8364) ? 128 : c;
		encrypted += String.fromCharCode(c+1);
	}
	return encrypted;
}

function decryptMail(mail){
	var decrypted = "";
	for(var i=0; i<mail.length; i++){
		var c = mail.charCodeAt(i);
		c = (c>=8364) ? 128 : c;
		decrypted += String.fromCharCode(c-1);
	}
	return decrypted;
}

function getMailto(encryptedMail){
	location.href="mailto:"+decryptMail(encryptedMail);
}

function createLink(mail){
	console.log("<a href=\"javascript:getMailto('"+encryptMail(mail)+"');\">"+mail.replace("@", "[at]")+"</a>");
}
