function addZoom(){
	$(".zoomable").click(function(){
		var background = createBG();
		var image = createImg($(this));
		var control = createCtrl($(this));
		var caption = createCaption($(this));
		//append all
		$("body").prepend(
			background.append(
				control,
				caption,
				image
			)
		);
	});
};

function createBG(){
	var background = $("<div></div>");
	background.css({
		"display": "table",
		"position": "fixed",
		"overflow": "hidden",
		"top": 0,
		"left": 0,
		"width": "100%",
		"height": "100%",
		"background": "rgba(0,0,0,0.85)",
		"z-index": 999,
		"font-family": "Arial",
	});
	background.click(function(){
		//$(this).remove();
	});
	return background;
};

function createImg(img){
	var newImg = $("<img>");
	newImg.attr({
		"src": img.attr("src"),
		"title": img.attr("title"),
		"alt": img.attr("alt")
	});
	newImg.css({
		"float": "none",
		"display": "block",
		"margin-top": "0px",
		"margin-left": "auto",
		"margin-right": "auto",
		"border": "5px solid #ffffff",
		"max-height": window.innerHeight-105+"px",
		"max-width": window.innerWidth-60+"px",
	});
	newImg.click(function(){
		$(this).parents().eq(0).remove();
	});
	return newImg;
};

function createCtrl(img){
	var ctrl = $("<div></div>");
	ctrl.css({
		"margin": "auto",
		"padding": "1px",
		"text-align": "center",
		"color": "#ffffff",
		"font-size": "50px",
		"font-weight": "bold",
	});
	var prevImg = $("<span id='prevImg' title='Show prevoius image.'> 🡄 </span>");
	var openImg = $("<span id='openImg' title='Open image in new tab.'> ❐ </span>");
	var closeImg = $("<span id='closeImg' title='Close image.'> × </span>");
	var nextImg = $("<span id='nextImg' title='Show next image.'> 🡆 </span>");
	prevImg.click(function(){
		var allZoomableImgs = $("html").find(".zoomable");
		var index = allZoomableImgs.index(img);
		$(this).parents().eq(1).remove();
		if(index>0){
			allZoomableImgs.eq(index-1).trigger("click");
		} else {
			allZoomableImgs.eq(allZoomableImgs.length-1).trigger("click");
		}
	});
	openImg.click(function(){
		window.open($(this).parents().eq(1).find("img").attr("src"), '_blank');
	});
	closeImg.click(function(){
		$(this).parents().eq(1).remove();
	});
	nextImg.click(function(){
		var allZoomableImgs = $("html").find(".zoomable");
		var index = allZoomableImgs.index(img);
		$(this).parents().eq(1).remove();
		if(index<allZoomableImgs.length-1){
			allZoomableImgs.eq(index+1).trigger("click");
		} else {
			allZoomableImgs.eq(0).trigger("click");
		}
	});
	ctrl.append(
		prevImg,
		openImg,
		closeImg,
		nextImg
	);
	return ctrl;
};

function createCaption(img){
	var allZoomableImgs = $("html").find(".zoomable");
	var pos = allZoomableImgs.index(img)+1;
	var title = (img.attr("caption")) ? img.attr("caption") : img.attr("title");
	var text = "("+pos+"/"+allZoomableImgs.length+") "+title;
	var caption = $("<div></div>");
	caption.css({
		"margin": "auto",
		"padding": "1px",
		"text-align": "center",
		"color": "#ffffff",
		"font-size": "16px",
	});
	caption.append(text);
	return caption;
};
